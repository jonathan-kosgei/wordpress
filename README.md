# Highly available Wordpress

## Getting Started

1. Install Docker

curl -fsSL get.docker.com | bash

2. Install docker-compose

**_Ubuntu_**
```
curl -L https://github.com/docker/compose/releases/download/$dockerComposeVersion/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

**_Mac_**
[Download Docker for Mac](https://docs.docker.com/docker-for-mac/install/#download-docker-for-mac)

3. Download and unzip Wordpress

wget https://wordpress.org/latest.zip
unzip latest.zip

## Deploy

Deploy Wordpress and Mariadb
```
cd site-a
docker-compose up -d --build
# The build option forces a rebuild of the image.
```

Deploy Nginx
```
cd nginx
docker-compose up -d
```

Wordpress will then be available on all interfaces at port 80

## Deploying multiple instances

To deploy a new wordpress site

Copy over the `docker-compose.yml` and the `Dockerfile` from `site-a` to a new folder `site-b`

**Important**

You need to rename the services in the wordpress docker-compose file

  web:
    build: .
    image: brett/wordpress:latest

  mariadb:
    image: mariadb
    environment:

This is because all the containers will share a network and each name is DNS resolvable and therefore needs to be unique.

Run through step 3 in Getting Started to install a fresh copy of wordpress

Copy the existing server block into the same http block like so

http {
  server {
    listen 0.0.0.0 default;
    location / {
            proxy_pass http://web;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
        }
  }
  server {
    server_name site-b.com; # respond to requests for site-b.com
    listen 0.0.0.0 default;
    location / {
            proxy_pass http://site-b-docker-compose-service-name; # This needs to be the same value the service name you set in the compose file
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_cache_bypass $http_upgrade;
        }
  }
  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;
}

Then restart nginx without downtime by running;

```
cd nginx
# exec into the nginx container
docker-compose exec nginx bash
# tell nginx to reload config
nginx -s reload
```

There will be a few seconds of downtime each time you add a host

## Sharing images

Running docker-compose --build in the site-a folder will create a wordpress image names `sitea_web:latest`

That's <parentfolder>_<wordpress_docker_compose_service_name>

If there's a dash in the parent folder name it will be removed.

You can then push sitea_web:latest to your private registry and have other developers pull it and run it.

You might need to tag the image with your registry username if you push to dockerhub.

For example if I'm pushing it to my account I'd do
```
docker tag sitea_web:latest jkosgei/sitea_wordpress_demo
```
and then push it with
```
docker push jkosgei/sitea_wordpress_demo
```

## Live Editing

Using the live.yml file will allow a developer to edit files and see the changes live.

When done with development and you'd like to create a docker image use

```
docker commit sitea_web sitea_image
docker tag sitea_image brett/sitea:version1.0
```

The first part "site" is the parent folder of the Docker Compose file the second